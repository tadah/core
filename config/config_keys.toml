[DBFILE]
default = []
value_type = ['string']
value_format = ['/path/to/dbfile ...']
value_N = [2147483647]
description = """Absolute or relative path to the database file. The relative path is to the script working directory. More than one dataset can be included, either by listing paths in the same line separated by spaces or by repeating the KEY multiple times."""
examples = ['DBFILE /path/to/dbfile', 'DBFILE /path/to/dbfile1 /path/to/dbfile2']
is_internal = [false]

[TYPE2B]
default = []
value_type = ['string']
value_format = ['D2_NAME']
value_N = [-2147483647]
description = """Type of a two-body descriptor to be used. Every two-body descriptor inherits from :cpp:class:`D2_Base`."""
examples = ['TYPE2B D2_LJ', 'TYPE2B D2_Blip']
is_internal = [false]

[TYPEMB]
default = []
value_type = ['string']
value_format = ['DM_NAME']
value_N = [-2147483647]
description = """Type of a many-body descriptor to be used. Every many-body descriptor inherits from :cpp:class:`DM_Base`."""
examples = ['TYPEMB DM_EAD']
is_internal = [false]

[RCUT2B]
default = []
value_type = ['double']
value_format = ['N']
value_N = [-2147483647]
description = """Cutoff distance used by the two-body descriptor."""
examples = ['RCUT2B 6.7']
is_internal = [false]

[RCUTMB]
default = []
value_type = ['double']
value_format = ['N']
value_N = [-2147483647]
description = """Cutoff distance used by the many-body descriptor."""
examples = ['RCUTMB 4.9']
is_internal = [false]

[RCTYPE2B]
default = []
value_type = ['string']
value_format = ['Cut_NAME']
value_N = [-2147483647]
description = """Cutoff type to be used with a two-body descriptor."""
examples = ['RCTYPE2B Cut_Cos']
is_internal = [false]

[RCTYPEMB]
default = []
value_type = ['string']
value_format = ['Cut_NAME']
value_N = [-2147483647]
description = """Cutoff type to be used with a many-body descriptor."""
examples = ['RCTYPEMB Cut_Cos']
is_internal = [false]

[MODEL]
default = []
value_type = ['string', 'string']
value_format = ['MODEL', 'FUNCTION']
value_N = [-3]
description = """This key defines the :code:`MODEL` to be used for training. :code:`MODEL` can be any class which inherits from :cpp:class:`M_Base`. :code:`FUNCTION` can be any child class of :cpp:class:`Function_Base`."""
examples = ['MODEL M_BLR BF_Linear', 'MODEL M_BLR BF_Polynomial2', 'MODEL M_KRR Kern_Linear']
is_internal = [false]

[MPARAMS]
default = []
value_type = ['double']
value_format = ['N1 N2 N3 ...']
value_N = [-2147483647]
description = """List of parameters used by some models. See model description for more details. Note that many models do not require this parameter at all."""
examples = ['MPARAMS 0.1']
is_internal = [false]

[CGRID2B]
default = []
value_type = ''
value_format = ''
value_N = [-2147483647]
description = """This KEY controls the position parameters used by the radial basis functions of a two-body descriptor, e.g., the position of a Gaussian function. The parameter list can be provided manually or generated automatically. This key is often used together with SGRID2B_. It is usually the case that both CGRID2B_ and SGRID2B_ must be the same size. In most cases, the maximum value should be smaller than the cutoff distance used for the two-body descriptor. Note that not all descriptors use this parameter, e.g., :cpp:class:`D2_LJ` has a fixed grid."""
examples = []
is_internal = [false]

[CGRIDMB]
default = []
value_type = ''
value_format = ''
value_N = [-2147483647]
description = """See CGRID2B_ for a description."""
examples = []
is_internal = [false]

[SGRID2B]
default = []
value_type = ''
value_format = ''
value_N = [-2147483647]
description = """Control the number of shape parameters for the radial basis functions for a two-body descriptor, e.g., the width of the Gaussian function. Similarly to CGRID2B_, the parameter list can be provided or generated automatically. This KEY is usually employed together with CGRID2B_."""
examples = []
is_internal = [false]

[SGRIDMB]
default = []
value_type = ''
value_format = ''
value_N = [-2147483647]
description = """See SGRID2B_ for a description."""
examples = []
is_internal = [false]

[OUTPREC]
default = [10]
value_type = ['int']
value_format = 'N'
value_N = [1]
description = """Number of decimal places used when dumping a potential file."""
examples = ['OUTPREC 12']
is_internal = [false]

# Remaining sections

[TYPE3B]
default = []
value_type = ['string']
value_format = ['D3_NAME']
value_N = [-2147483647]
description = """Dummy. See INIT3B_."""
examples = []
is_internal = [false]

[RCTYPE3B]
default = []
value_type = ['string']
value_format = ['Cut_NAME']
value_N = [-2147483647]
description = """Dummy. See INIT3B_."""
examples = []
is_internal = [false]

[RCUT3B]
default = []
value_type = ['double']
value_format = ['N']
value_N = [-2147483647]
description = """Dummy. See INIT3B_."""
examples = []
is_internal = [false]

[INIT2B]
default = [false]
value_type = ['bool']
value_format = ['true | false']
value_N = [1]
description = """If set to true, the two-body descriptor will be calculated."""
examples = ['INIT2B true']
is_internal = [false]

[INIT3B]
default = [false]
value_type = ['bool']
value_format = ['true | false']
value_N = [1]
description = """This is a dummy flag as Ta-dah! does not calculate three-body descriptors. Three-body interactions can be included with some of the many-body descriptors."""
examples = []
is_internal = [false]

[INITMB]
default = [false]
value_type = ['bool']
value_format = ['true | false']
value_N = [1]
description = """If set to true, the many-body descriptor will be calculated."""
examples = ['INITMB true']
is_internal = [false]

[SEMBFUNC]
default = []
value_type = ['double']
value_format = ['N1 N2 N3 ...']
value_N = [-2147483647]
description = """A number of shape parameters of the embedding function. Used by some many-body descriptors, it controls the depth of the function in :cpp:class:`F_RLR`."""
examples = ['SEMBFUNC 0.14 0.45 1.00 1.1']
is_internal = [false]

[CEMBFUNC]
default = []
value_type = ['double']
value_format = ['N1 N2 N3 ...']
value_N = [-2147483647]
description = """A number of position parameters of the embedding function. Used by some many-body descriptors, it controls where the x-intercept is in :cpp:class:`F_RLR`."""
examples = ['CEMBFUNC 0.14 0.45 1.00 1.1']
is_internal = [false]

[FORCE]
default = [false]
value_type = ['bool']
value_format = ['true | false']
value_N = [1]
description = """Set to true to calculate force descriptors and/or use forces during the training process."""
examples = []
is_internal = [false]

[STRESS]
default = [false]
value_type = ['bool']
value_format = ['true | false']
value_N = [1]
description = """Set to true to calculate stress descriptors and/or use virial stress during the training process."""
examples = []
is_internal = [false]

[SBASIS]
default = []
value_type = ['int']
value_format = ['N']
value_N = [1]
description = """The number of basis functions to use when constructing the :cpp:class:`DesignMatrix`. Note that many models do not require this parameter at all."""
examples = ['SBASIS 10']
is_internal = [false]

[NORM]
default = [false]
value_type = ['bool']
value_format = ['true | false']
value_N = [1]
description = """Set to true to standardise descriptors. Note that this usually makes sense only when energies are used for fitting."""
examples = ['NORM true']
is_internal = [false]

[LAMBDA]
default = [0]
value_type = ['int | double | int double']
value_format = ['N | N M']
value_N = [-2]
description = """This KEY controls the regularisation parameter :math:`\\lambda` for both :cpp:class:`M_BLR` and :cpp:class:`M_KRR`. If :code:`N=0`, no regularisation is applied. If :code:`N>0`, :math:`\\lambda` is set to this value. If :code:`N<0`, an evidence-approximation algorithm estimates the value of :math:`\\lambda`. For LAMBDA 0, second number, M (double), can be specified, i.e. LAMBDA 0 1e-12, which helps to determine the effective rank of matrix ::math:`\\Phi` (default 1e-8)."""
examples = ['LAMBDA -1', 'LAMBDA 1e-4', 'LAMBDA 0', 'LAMBDA 0 1e-12']
is_internal = [false]

[ALPHA]
default = [1.0]
value_type = ['double']
value_format = ['N']
value_N = [1]
description = """Weight precision hyper-parameter. Starting guess used in the evidence approximation algorithm."""
examples = ['ALPHA 0.23']
is_internal = [false]

[BETA]
default = [1.0]
value_type = ['double']
value_format = ['N']
value_N = [1]
description = """Noise precision hyper-parameter. Starting guess used in the evidence approximation algorithm."""
examples = ['BETA 0.0001']
is_internal = [false]

[SETFL]
default = []
value_type = ['string']
value_format = ['/path/to/dbfile']
value_N = [1]
description = """Path to the setfl file with the EAM potential."""
examples = ['SETFL Ta1_Ravelo_2013.eam.alloy']
is_internal = [false]

[BIAS]
default = [true]
value_type = ['bool']
value_format = ['true | false']
value_N = [1]
description = """Controls whether to append 1 to every descriptor. Increases DSIZE by 1."""
examples = ['BIAS false']
is_internal = [false]

[VERBOSE]
default = [0]
value_type = ['int']
value_format = 'N'
value_N = [1]
description = """Set verbosity level. For :code:`N>0`, it provides detailed output for diagnostic purposes."""
examples = ['VERBOSE 1']
is_internal = [false]

[EWEIGHT]
default = [1.0]
value_type = ['double']
value_format = ['N']
value_N = [1]
description = """Global energy scaling factor for all configurations used in the training process. Note that energies are always scaled by 1/number of atoms. Individual scaling factors for every configuration can be set in a dataset file. The combined scaling factor is: EWEIGHT*(configuration eweight)/(number of atoms)"""
examples = ['EWEIGHT 0.96']
is_internal = [false]

[FWEIGHT]
default = [1.0]
value_type = ['double']
value_format = ['N']
value_N = [1]
description = """Global force scaling factor for all configurations used in the training process. Note that each force component is always scaled by 1/(number of atoms)/3. Individual scaling factors for every configuration can be set in a dataset file. The combined scaling factor is: FWEIGHT*(configuration fweight)/(number of atoms)/3"""
examples = ['FWEIGHT 1e-2']
is_internal = [false]

[SWEIGHT]
default = [1.0]
value_type = ['double']
value_format = ['N']
value_N = [1]
description = """Global stress scaling factor for all configurations used in the training process. Note that each stress component is always scaled by 1/6. Individual scaling factors for every configuration can be set in a dataset file. The combined scaling factor is: SWEIGHT*(configuration sweight)/6"""
examples = ['SWEIGHT 1e-1']
is_internal = [false]

[WEIGHTS]
default = []
value_type = ['double']
value_format = ['N1 N2 N3 ...']
value_N = [-2147483647]
description = """The machine-learned coefficients for the model, obtained during the optimisation process. These are species-dependent weights and can be set by the user. The default weight is an atomic number."""
examples = ['WEIGHTS 0.12 1.2 0.3']
is_internal = [false]

[SIGMA]
default = []
value_type = ['int', 'double']
value_format = ['N', 'D ...']
value_N = [-2147483647]
description = """The :math:`\\Sigma` matrix used in Bayesian Linear Regression :cpp:class:`M_BLR`. The matrix is :math:`N\\times N` and stored in column-major order."""
examples = ['`Sigma 2 1.2 2.2 2.3 3.3`']
is_internal = [false]

[NSTDEV]
default = []
value_type = ['double']
value_format = ['N1 N2 N3 ...']
value_N = [-2147483647]
description = """Standard deviations obtained during standardisation (see NORM_) of the columns of the :cpp:class:`DesignMatrix`. The size of the vector is equal to the number of columns."""
examples = []
is_internal = [false]

[NMEAN]
default = []
value_type = ['double']
value_format = ['N1 N2 N3 ...']
value_N = [-2147483647]
description = """Mean values for the columns of the :cpp:class:`DesignMatrix`. This vector is obtained during standardisation (see NORM_)."""
examples = []
is_internal = [false]

[BASIS]
default = []
value_type = ['double']
value_format = ['N1 N2 N3 ...']
value_N = [-2147483647]
description = """Basis vectors used by the non-linear Kernel Ridge Regression model. In this context, they represent the features or functions used to map input data into a higher-dimensional feature space to capture non-linear relationships."""
examples = []
is_internal = [false]

[DIMER]
default = [false, 0, false]
value_type = ['bool', 'double', 'bool']
value_format = ['F', 'BOND_LENGTH', 'B']
value_N = [-3]
description = """Control for DIMER models. Users should not modify this key."""
examples = ['DIMER true 1.104 true']
is_internal = [false]

[WATOMS]
default = []
value_type = ['double']
value_format = 'N'
value_N = [-2147483647]
description = """Weights sorted by the atomic number of the corresponding atom, with the lowest Z value first. WATOMS.size() == ATOMS.size()"""
examples = []
is_internal = [false]

[ATOMS]
default = []
value_type = ['string']
value_format = 'N'
value_N = [-2147483647]
description = """List of unique atoms sorted by Z. This key is set by the library. Users should not set it."""
examples = []
is_internal = [false]

[SIZE2B]
default = []
value_type = ['int']
value_format = 'N'
value_N = [1]
description = """Size of 2B descriptor."""
examples = []
is_internal = [true]

[SIZE3B]
default = []
value_type = ['int']
value_format = 'N'
value_N = [1]
description = """Size of 3B descriptor."""
examples = []
is_internal = [true]

[SIZEMB]
default = []
value_type = ['int']
value_format = 'N'
value_N = [1]
description = """Size of MB descriptor."""
examples = []
is_internal = [true]

[DSIZE]
default = []
value_type = ['int']
value_format = 'N'
value_N = [1]
description = """The total size of the descriptor. 2B + 3B + MB + bias."""
examples = []
is_internal = [true]

[RCUTMAX]
default = []
value_type = ['int']
value_format = 'N'
value_N = [1]
description = """Tadah internal key."""
examples = []
is_internal = [true]

[RCUT2BMAX]
default = []
value_type = ['int']
value_format = 'N'
value_N = [1]
description = """Tadah internal key."""
examples = []
is_internal = [true]

[RCUT3BMAX]
default = []
value_type = ['int']
value_format = 'N'
value_N = [1]
description = """Tadah internal key."""
examples = []
is_internal = [true]

[RCUTMBMAX]
default = []
value_type = ['int']
value_format = 'N'
value_N = [1]
description = """Tadah internal key."""
examples = []
is_internal = [true]

[ESTDEV]
default = []
value_type = ['int']
value_format = 'N'
value_N = [1]
description = """Tadah internal key."""
examples = []
is_internal = [true]

[FSTDEV]
default = []
value_type = ['int']
value_format = 'N'
value_N = [1]
description = """Tadah internal key."""
examples = []
is_internal = [true]

[SSTDEV]
default = []
value_type = ['int']
value_format = 'N'
value_N = [-6]
description = """Tadah internal key."""
examples = []
is_internal = [true]

[MPIWPCKG]
default = [50]
value_type = ['int']
value_format = 'N'
value_N = [2147483647]
description = """The number of structures in a single MPI work package."""
examples = []
is_internal = [false]

[MBLOCK]
default = [64]
value_type = ['int']
value_format = 'N'
value_N = [2147483647]
description = """ScalaPACK row block size MB."""
examples = []
is_internal = [false]

[NBLOCK]
default = [64]
value_type = ['int']
value_format = 'N'
value_N = [2147483647]
description = """ScalaPACK column block size NB."""
examples = []
is_internal = [false]

[FIXWEIGHT]
default = []
value_type = ['double']
value_format = ['w1 w2 w3 ...']
value_N = [2147483647]
description = """Values for weights to be fixed during the optimisation process. Requires FIXINDEX."""
examples = ["1.0 1.0 0.5"]
is_internal = [false]

[FIXINDEX]
default = []
value_type = ['int']
value_format = ['N1 N2 N3 ...']
value_N = [2147483647]
description = """Indices of weights to be fixed during the optimisation process. Requires FIXWEIGHT."""
examples = ["1 4 5"]
is_internal = [false]

