#include <tadah/core/config.h>
#include <sstream>

Config::Config()
{
  read_tadah_configuration();
  set_defaults();
  postprocess();
}
/*
Config::Config(const Config &other)
{
    std::cout << "Copy constructor" << std::endl;
    *this = other;
    set_defaults();
    postprocess();
}
Config& Config::operator=(const Config& other)
{
    std::cout << "Copy assignment 1" << std::endl;
    if(this != &other) {
        std::cout << "Copy assignment 2" << std::endl;
        //read_tadah_configuration();
        c = other.c;
        tdc = other.tdc;
        set_defaults();
        postprocess();
    }
    return *this;
}
*/
Config::Config(std::string fn) {

    read_tadah_configuration();
    read(fn);
    set_defaults();
    postprocess();
}

void Config::read(std::string fn) {
    std::ifstream ifs(fn);
    if (!ifs.good())
        throw std::runtime_error("Configuration file does not exist:\n"+fn+"\n");
    std::string s;

    while(std::getline(ifs,s)) {
        std::istringstream iss(s);
        std::string key, value;
        iss >> key >> value;

        // skip empty lines
        if(key.empty()) continue;

        // skip comments starting with #
        if(key[0]=='#') continue;

        // enforce UPPERCASE KEY
        for (auto & k: key) k = std::toupper(k);

        check(key,value);
        c[key].push_back(value);
        while (iss >> value) {
            if (value[0]=='#') break;
            c[key].push_back(value);
        }
    }
    ifs.close();
  postprocess();
}

void Config::postprocess() {
  // calculate and set max cutoff distance
  double c2b=0, c3b=0,cmb=0;
  double temp=0;
  int n= exist("OUTPREC") ? get<int>("OUTPREC") : 6;
  c.erase("RCUT2BMAX");
  c.erase("RCUT3BMAX");
  c.erase("RCUTMBMAX");
  c.erase("RCUTMAX");

  if (c.count("INIT2B") && (*this).get<bool>("INIT2B") && c.count("RCUT2B")) {
    for (size_t i = 0; i < size("RCUT2B"); ++i) {
      double temp = get<double>("RCUT2B", i);
      if (temp > c2b) c2b = temp;
    }
  }
  if (c.count("INIT3B") && (*this).get<bool>("INIT3B") && c.count("RCUT3B")) {
    for (size_t i=0; i<size("RCUT3B"); ++i) {
      temp=get<double>("RCUT3B",i);
      if(temp>c3b) c3b = temp;
    }
  }
  if (c.count("INITMB") && (*this).get<bool>("INITMB") && c.count("RCUTMB")) {
    for (size_t i=0; i<size("RCUTMB"); ++i) {
      temp=get<double>("RCUTMB",i);
      if(temp>cmb) cmb = temp;
    }
  }
  if (c2b) c["RCUT2BMAX"]= std::vector<std::string>(1,to_string(c2b,n));
  if (c3b) c["RCUT3BMAX"]= std::vector<std::string>(1,to_string(c3b,n));
  if (cmb) c["RCUTMBMAX"]= std::vector<std::string>(1,to_string(cmb,n));
  double max = std::max(cmb,std::max(c2b,c3b));
  if (max) c["RCUTMAX"]= std::vector<std::string>(1,to_string(max,n));
}

bool Config::check(std::string &key, std::string &value) { 
  // 1.0 Check is key allowed
  if (!tdc.contains(key))
    throw std::runtime_error("Disallowed key "+key+" "+value);
  // 2.0 Check if user is permitted to set this key
  if ((internal_keys.count(key)>0))
    throw std::runtime_error("User cannot set this key.\n\
This key is used internally\n\
and its value is calculated by the library "+key+" "+value);
  // 3.0 Check for duplicate keys
  const auto& allowed_key = toml::find(tdc, key);
  int valN  = toml::find<int>(allowed_key, "value_N",0);
  if (c[key].size() >= (size_t)abs(valN))
    throw std::runtime_error("Repeated key "+key+" "+value);

  // end of checks, proceed...

  return true;
}
bool Config::check_for_training() {
    if (!exist("DBFILE")) {
        throw std::runtime_error("DBFILE not specified. Check your settings.\n");
    }
    if (!(*this).get<bool>("INIT2B") &&
        !(*this).get<bool>("INIT3B") &&
        !(*this).get<bool>("INITMB")) {
        throw std::runtime_error("At least one type must be initialised (true) \n\
                in the config object: INIT2B, INIT3B, INITMB.\n");
    }

    if (!c.count("TYPE2B") && !c.count("TYPE3B")
            && !c.count("TYPEMB") )
        throw std::runtime_error("At least one type must be specified \n\
                in the config object: TYPE2B, TYPE3B, TYPEMB.\n");

    if (!c.count("RCTYPE2B") && !c.count("RCTYPE3B")
            && !c.count("RCTYPEMB") )
        throw std::runtime_error("At least one type must be specified \n\
                in the config object: RCTYPE2B, RCTYPE3B, RCTYPEMB.\n");

    if (!exist("RCUT2B") && !exist("RCUT3B")
            && !exist("RCUTMB") )
        throw std::runtime_error("At least one type must be specified \n\
                in the config object: RCUT2B, RCUT3B, RCUTMB.\n");

     if (get<double>("RCUTMAX")<=0)
         throw std::runtime_error("RCUTMAX cannot be set. Check your settings.\n");

    return true;
}
bool Config::check_for_predict() {
    return true;
}
bool Config::check_pot_file() {
    return true;
}
void Config::set_defaults() {
  for (const auto& kv : tdc.as_table()) {
    auto k = kv.first;
    auto v = kv.second;
    std::string key = k;

    if (c.count(key) && !c[key].empty()) { 
      continue;
    } // the value is already set

    if (!v.contains("default") || !v["default"].is_array()) {
      continue;
    }

    auto def  = toml::find(v, "default").as_array();

    if (def.empty()) continue;

    for (auto &el : def) {
      if (el.is_string()) {
        c[key].push_back(el.as_string());
      }
      if (el.is_integer()) {
        c[key].push_back(to_string(el.as_integer()));
      }
      if (el.is_boolean()) {
        c[key].push_back(el.as_boolean() ? "true" : "false");
      }
      if (el.is_floating()) {
        c[key].push_back(to_string(el.as_floating(),16));
      }
    }
  }
}
void Config::clear_internal_keys() {
  for (auto it = c.cbegin(); it != c.cend() /* not hoisted */; /* no increment */)
  {
    if (internal_keys.count(it->first)>0) {
      c.erase(it++);    // or "it = m.erase(it)" since C++11
    }
    else {
      ++it;
    }
  }
}
const std::vector<std::string>& Config::operator()(const std::string key) const {
    try {
        if (c.count(to_upper(key)) == 0)
            throw std::runtime_error(knf + to_upper(key));
        return c.at(to_upper(key));
    } catch (const std::exception &e) {
        std::cerr << "Error accessing key '" << key << "': " << e.what() << "\n";
        throw;
    } catch (...) {
        std::cerr << "Unknown error accessing key '" << key << "'.\n";
        throw;
    }
}

size_t Config::size(const std::string key) const {
    try {
        if (c.count(to_upper(key)) == 0)
            throw std::runtime_error(knf + to_upper(key));
        return c.at(to_upper(key)).size();
    } catch (const std::exception &e) {
        std::cerr << "Error getting size for key '" << key << "': " << e.what() << "\n";
        throw;
    } catch (...) {
        std::cerr << "Unknown error getting size for key '" << key << "'.\n";
        throw;
    }
}

bool Config::exist(const std::string key) const {
    try {
        return c.count(to_upper(key));
    } catch (const std::exception &e) {
        std::cerr << "Error checking existence of key '" << key << "': " << e.what() << "\n";
        return false;
    } catch (...) {
        std::cerr << "Unknown error checking existence of key '" << key << "'.\n";
        return false;
    }
}
const std::map<std::string,int> Config:: internal_keys =  Config::create_internal_keys();
const std::string Config::knf = "Key not found: ";
bool Config::operator==(const Config &ctest) const {
    return c==ctest.c;
}
size_t Config::remove(const std::string key) {
  size_t rv =  c.erase(to_upper(key));
  postprocess();
  return rv;
}
void Config::add(const std::string fn) {
    read(fn);
    set_defaults();
    postprocess();
}
void Config::read_tadah_configuration() {
    auto fs = cmrc::CORE::get_filesystem();
    auto toml_file = fs.open("config/config_keys.toml");
    std::stringstream ss;
    ss << toml_file.begin();
    try
    {
        tdc = toml::parse(ss);
    }
    catch (const std::runtime_error& err)
    {
        std::cerr << "Parsing of tadah configuration has failed:\n"
            << "This is a BUG!\n" << std::endl;
        throw err;
    }
}
void Config::clear() {
  c.clear();
}

// Serialize the map to a vector of chars
std::vector<char> Config::serialize() {
    std::vector<char> buffer;

    for (const auto& item : c) {
        const std::string& key = item.first;
        const std::vector<std::string>& vec = item.second;

        size_t keySize = key.size();
        buffer.insert(buffer.end(),
                      reinterpret_cast<const char*>(&keySize),
                      reinterpret_cast<const char*>(&keySize) + sizeof(keySize));
        buffer.insert(buffer.end(), key.begin(), key.end());

        size_t vecSize = vec.size();
        buffer.insert(buffer.end(),
                      reinterpret_cast<const char*>(&vecSize),
                      reinterpret_cast<const char*>(&vecSize) + sizeof(vecSize));

        for (const std::string& str : vec) {
            size_t strSize = str.size();
            buffer.insert(buffer.end(),
                          reinterpret_cast<const char*>(&strSize),
                          reinterpret_cast<const char*>(&strSize) + sizeof(strSize));
            buffer.insert(buffer.end(), str.begin(), str.end());
        }
    }

    return buffer;
}

// Deserialize the map from a vector of chars
void Config::deserialize(const std::vector<char>& buffer) {
    
    size_t pos = 0;

    while (pos < buffer.size()) {
        size_t keySize;
        std::memcpy(&keySize, &buffer[pos], sizeof(keySize));
        pos += sizeof(keySize);

        std::string key(buffer.begin() + pos, buffer.begin() + pos + keySize);
        pos += keySize;

        size_t vecSize;
        std::memcpy(&vecSize, &buffer[pos], sizeof(vecSize));
        pos += sizeof(vecSize);

        std::vector<std::string> vec;
        for (size_t i = 0; i < vecSize; ++i) {
            size_t strSize;
            std::memcpy(&strSize, &buffer[pos], sizeof(strSize));
            pos += sizeof(strSize);

            std::string str(buffer.begin() + pos, buffer.begin() + pos + strSize);
            pos += strSize;

            vec.push_back(str);
        }

        c[key] = vec;
    }

}

